A script that pulls from the scryfall API, obtaining card names for all commons and uncommons in a given set, and compiling them into decklists to import into MTG Arena.

Usage:

Run the script, with the the set code as a command line argument. The lists will be output to list.txt in the same directory.

Example:

python3 pull.py snc
