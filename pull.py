import requests
import argparse
import json

#command line argument parser
p = argparse.ArgumentParser()
p.add_argument("set", help="enter a valid set code")
args = p.parse_args()
#print(args.set)

r = requests.get("https://api.scryfall.com/cards/search?q=s%3A"+args.set+"+r%3Cr&unique=cards&as=grid&order=name")

r = r.json()

f = open("list.txt","w")

cardCount = 0
f.write("Deck\n")
for l in r["data"]:
    f.write("4 "+ l["name"]+"\n")
    
    if cardCount>=240:
        cardCount = 0
        f.write("\n\nDeck\n")
    else:
        cardCount += 4

f.close()